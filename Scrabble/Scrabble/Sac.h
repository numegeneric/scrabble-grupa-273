#pragma once
#include <iostream>
#include <vector>
#include<random>
#include<tuple>
#include<time.h>
#include<string>
#include"Piesa.h"

class Piesa;
int const nrLetters = 26;

class Sac
{
public:
	Sac();
	~Sac();
	void afisarePiese();
	Piesa* getPiesa();
	int getnrPiese()const;
	void addPiesa(Piesa*);
	void reset();
	Piesa* getPiesa(std::string litera);
private:
	std::vector<std::pair<Piesa, int>> m_piese;
	int m_nrPiese;
};

