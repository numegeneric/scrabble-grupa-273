#pragma once
#include <iostream>
#include <string>
#include"Zona.h"

class Zona;

class Piesa
{
public:
	Piesa(std::string, int);
	~Piesa();
	Zona* arataZona()const;
	std::string getLitera();
	int getScor();
	void setZona(Zona*);
	bool operator ==(Piesa other);
private:
	std::string m_litera;
	int m_scor;
	Zona* m_zona;
};

