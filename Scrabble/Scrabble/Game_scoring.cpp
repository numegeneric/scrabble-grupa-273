#include <iostream>
#include <cctype>
#include <string>
#include <map>
#include "Game_scoring.h"

int main()
{
	while (true)
	{
		// Using a string instead of a fixed size array.
		std::string word;
		std::cout << "Enter a word: ";
		std::getline(std::cin, word);

		// Test if the input word is empty or not.
		if (word.empty())
		{
			break;
		}

		const int score = gameScore(word);
		std::cout << "The score for '" << word << "' is " << score << "\n";
	}

	std::cin.get();
}