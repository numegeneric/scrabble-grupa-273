#pragma once

enum Multiplicatori {
	DOUBLE_LETTER,
	TRIPLE_LETTER,
	DOUBLE_WORD,
	TRIPLE_WORD,
	NONE
};