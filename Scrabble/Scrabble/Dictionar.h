#pragma once
#include<set>
#include<fstream>
#include<string>
class Dictionar
{
public:
	Dictionar();
	Dictionar(const int&);

	~Dictionar();

	bool checkWord(const std::string& word);


private:
	std::set<std::string> m_Words;
};

