
#include<iostream>
#include<vector>
#include<string>
#include "Player.h"
#include "Sac.h"
#include "TablaJoc.h"
#include "Piesa.h"
#include "Tablita.h"
#include <cstring>
#include"Dictionar.h"
const int kNrPieseTablita = 7;
const int rowSize = 15;
const int kDexValue = 1;
Dictionar dictionar(kDexValue);
Player::Player(std::string s, TablaJoc& tablaJoc, Sac& sac) :
	scor(0),
	name(s),
	tablaJoc(tablaJoc),
	sac(sac)
{
	tablita = new Tablita(sac);

	fillTablita();

}

Player::~Player()
{
	delete tablita;
}

std::string Player::getName()
{
	return name;
}

bool Player::poatePuneLitera(std::string litere, int index, direction dir)
{
	//verificam daca directia este buna
	if (dir == NODIR)
	{
		std::cout << "Wrong Direction !" << std::endl;
		return false;
	}

	//cuv prea lung
	if (litere.size() > kNrPieseTablita)
	{
		std::cout << "The word is to long !" << std::endl;
		return false;
	}

	// verificam daca avem toate literele in tablita
	if (!tablita->hasCaractere(litere))
	{
		std::cout << "Invalid piecies !" << std::endl;
		return false;
	}

	if (index < 0 || index > 224)
	{
		std::cout << "Invalid index ! " << std::endl;
		return false;
	}

	if (!tablaJoc.poatePuneLitera(litere, index, dir))
	{
		std::cout << "Can't attache the letters !" << std::endl;
		return false;
	}

	return true;
}

struct move Player::ataseazaLitere(std::string litere, int index, direction dir)
{
	struct move m;
	int temp, mainScor = 0, mainMultiplier = 1, current, tempScor, tempMultiplier;
	unsigned int pos = 0;
	std::string mainStr = "", str1, str2;

	//verificam literele din urma
	if (dir == HORIZONTAL) //stanga
	{
		temp = index - 1;
		while (temp / rowSize == index / rowSize && tablaJoc.getPiesa(temp))
		{
			mainScor += tablaJoc.getPiesa(temp)->getScor();
			mainStr = tablaJoc.getPiesa(temp)->getLitera() + mainStr;
			temp--;
		}
	}
	else //above
	{
		temp = index - rowSize;
		while (temp >= 0 && tablaJoc.getPiesa(temp))
		{
			mainScor += tablaJoc.getPiesa(temp)->getScor();
			mainStr = tablaJoc.getPiesa(temp)->getLitera() + mainStr;
			temp -= rowSize;
		}
	}

	//incepem sa punem piese pe tabla
	current = index;
	while (1)
	{
		if (current > 224 || (dir == HORIZONTAL && (current / rowSize != index / rowSize))) //nu se afla in zona tablei de joc
			break;

		if (tablaJoc.getPiesa(current))
		{
			mainScor += tablaJoc.getPiesa(current)->getScor();
			mainStr += tablaJoc.getPiesa(current)->getLitera();
		}
		else if (pos < litere.size())//atasam litera
		{
			std::string litera;
			litera = litere[pos];
			Piesa* t = tablita->getPiesa(litera);
			pos++;
			Multiplicatori p = tablaJoc.getMultiplicator(current);
			switch (p) {
			case DOUBLE_LETTER: tempScor = t->getScor() * 2; tempMultiplier = 1; break;
			case TRIPLE_LETTER: tempScor = t->getScor() * 3; tempMultiplier = 1;  break;
			case DOUBLE_WORD: tempScor = t->getScor(); tempMultiplier = 2; break;
			case TRIPLE_WORD: tempScor = t->getScor(); tempMultiplier = 3; break;
			case NONE: tempScor = t->getScor(); tempMultiplier = 1; break;
			}
			mainScor += tempScor;
			mainMultiplier *= tempMultiplier;

			tablaJoc.adaugalaZona(current, t); //adaugam piesa
			mainStr += t->getLitera();


			str1 = ""; str1 += t->getLitera();
			str2 = "";

			//dupa ce adaugam litera ne miscam sus,jos,stanga,dreapta ca sa formam cuvantul, dupa adaugam scorul
			if (dir == HORIZONTAL)
			{
				//sus
				temp = current - rowSize;
				while (temp >= 0 && tablaJoc.getPiesa(temp))
				{
					tempScor += tablaJoc.getPiesa(temp)->getScor();
					str1 = tablaJoc.getPiesa(temp)->getLitera() + str1;
					temp -= rowSize;
				}
				//jos
				temp = current + rowSize;
				while (temp < (rowSize+1)*(rowSize+1) && tablaJoc.getPiesa(temp))
				{
					tempScor += tablaJoc.getPiesa(temp)->getScor();
					str2 += tablaJoc.getPiesa(temp)->getLitera();
					temp += rowSize;
				}
			}
			else
			{
				//stanga
				temp = current - 1;
				while (temp / rowSize == current / rowSize && tablaJoc.getPiesa(temp))
				{
					tempScor += tablaJoc.getPiesa(temp)->getScor();
					str1 = tablaJoc.getPiesa(temp)->getLitera() + str1;
					temp--;
				}
				//dreapta
				temp = current + 1;
				while (temp / rowSize == current / rowSize && tablaJoc.getPiesa(temp))
				{
					tempScor += tablaJoc.getPiesa(temp)->getScor();
					str2 += tablaJoc.getPiesa(temp)->getLitera();
					temp++;
				}
			}
			if (str1.size() + str2.size() > 1)
			{
				m.words.push_back(str1 + str2);
				m.scores.push_back(tempScor*tempMultiplier);
			}
		}
		else break;

		if (dir == HORIZONTAL) current++;
		else current += rowSize;
	}

	m.words.push_back(mainStr);
	m.scores.push_back(mainScor * mainMultiplier);
	m.ataseazaLitere = litere;

	return m;
}

void Player::fillTablita()
{
	Piesa* t;
	while (tablita->getNumPiese() < kNrPieseTablita && (t = sac.getPiesa()) != nullptr)
	{
		tablita->addPiesa(t);
	}

}

void Player::stergeTabla(std::string & cuvant, int & indexLocatie, char & directie)
{
	direction dir = charToDirection(directie);
	int pos = 0;
	std::string cuvantSters = "";
	while (true)
	{
		if (cuvantSters == cuvant)
			break;
		Piesa* piesa = tablaJoc.getPiesa(indexLocatie);
		std::string litera;
		litera = cuvant[pos];
		if (piesa->getLitera() == litera)
		{
			piesa->setZona(nullptr);
			tablaJoc.stergelaZona(indexLocatie);
			tablita->addPiesa(piesa);
			pos++;
			cuvantSters.append(litera);
			if (dir == HORIZONTAL)
				indexLocatie++;
			else
				indexLocatie += rowSize;
		}
		else
		{
			if (dir == HORIZONTAL)
				indexLocatie++;
			else
				indexLocatie += rowSize;
		}
	}
}

void Player::Joaca()
{
	tablita->show();
	bool stareFirstWord;
	stareFirstWord = tablaJoc.primulCuvant();
	std::string letterString;
	int locationNdx;
	char dir;

	getInput(letterString, locationNdx, dir);

	if (!poatePuneLitera(letterString, locationNdx, charToDirection(dir)))
	{
		std::cout << "The letters can't be physically attached !" << std::endl;
		return;
	}
	else
		std::cout << "The letters can be physically attached !" << std::endl;

	struct move m = ataseazaLitere(letterString, locationNdx, charToDirection(dir));

	bool validWords=true;
	Dictionar dictionar(1);
	int score = 0;
	std::cout << "Possible words: " << std::endl;
	for (unsigned int i = 0; i < m.words.size(); i++)
	{
		if (dictionar.checkWord(m.words[i]) != true)
			validWords = false;
		std::cout << " --> " << m.words[i] << " " << m.scores[i] << std::endl;
		score += m.scores[i];
	}
	if (validWords == true)
	{
		std::cout << "The words are valid ! " << std::endl;

		std::cout << "Round Score: " << score << std::endl;
		fillTablita();

		moves.push_back(m);
	}
	else
	{
		std::cout << "The words are not valid ! " << std::endl;
		stergeTabla(letterString, locationNdx, dir);
		if (stareFirstWord != tablaJoc.primulCuvant())
			tablaJoc.setPrimulCuvant(false);
	}
}

direction Player::charToDirection(char c)
{
	switch (c) {
	case 'o':
		return HORIZONTAL;
	case 'v':
		return VERTICAL;
	default:
		return NODIR;
	}
}

void Player::getInput(std::string& litere, int& locationNdx, char& direction)
{

	do {


		std::cout << "Insert a word using the letters on the rack :";
		std::cin >> litere;
		std::cout << "Insert the starting point of the word(0-224): ";
		std::cin >> locationNdx;
		std::cout << "Insert the orientation of the word (horizontal('o') or vertical('v')) : ";
		std::cin>> direction;
		std::cout << std::endl;
		std::cin.clear();
	} while (locationNdx < 0 || locationNdx > 224 || (direction != 'o' && direction != 'v'));
}

