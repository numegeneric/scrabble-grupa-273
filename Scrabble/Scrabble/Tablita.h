
#pragma once
#include<iostream>
#include<iomanip>
#include<string>
#include "Piesa.h"
#include "Sac.h"
#include "Culori.h"
class Piesa;
class Sac;


class Tablita {
private:
	std::vector<Piesa*> piese;
	const int kTablitaSize = 7;


public:
	Tablita();
	Tablita(Sac&);
	~Tablita();

	void show() const;
	int getNumPiese() const;
	bool addPiesa(Piesa* t);

	bool hasPiesa(const std::string& litera);
	Piesa* getPiesa(const int& index);
	Piesa* getPiesa(const std::string& c);

	Piesa* showPiesa(const int& index) const;
	Piesa* showPiesa(const std::string& c) const;

	std::string getCaractere() const;

	bool hasCaractere(const std::string& litera) const;

	void selfTest() const;
};



