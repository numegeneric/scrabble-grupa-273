#include<iostream>
#include<vector>
#include "Joc.h"
#include "player.h"
#include "Sac.h"
#include "TablaJoc.h"
#include"Dictionar.h"


Joc::Joc()
{
	tablaJoc = new TablaJoc;
	sac = new Sac;

	int xvalue = 0;
	std::cout << "Limbi disponibile:\n";
	std::cout << "1.Engleza\n";
	std::cout << "2.Romana\n";
	do
	{
		std::cout << "Alege limba jocului : ";
		std::cin >> xvalue;
		if (xvalue < 0 || xvalue>2)
			std::cout << "Input incorect." << std::endl;
	} while ((xvalue < 0) || (xvalue > 2));


	std::cout << "Insert the number of players (2-4): ";
	int nrJucatori = 0;
	while ((nrJucatori < 2) || (nrJucatori > 4))
	{
		std::cin >> nrJucatori;
	}
	for (int index = 0; index < nrJucatori; index++)
	{
		std::cout << "Insert the name of the player: ";
		std::string nume;
		std::cin >> nume;
		players.push_back(new Player(nume, *tablaJoc, *sac));
	}
}

Joc::~Joc()
{
	delete tablaJoc;
	delete sac;

	for (auto p : players)
		delete p;
}

void Joc::begin()
{


	while (true)
	{
		for (int i = 0; i < players.size(); i++)
		{
			//system("cls");
			tablaJoc->arataTabla();
			std::cout << "Is " << players[i]->getName() << "'s turn !" << std::endl;
			players[i]->Joaca();
		}
	}

}


