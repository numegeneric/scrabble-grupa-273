#pragma once



#ifndef PLAYER_H
#define PLAYER_H

#include<string>
#include<vector>
#include<iostream>
#include <fstream>
#include"Dictionar.h"

class Tablita;
class Sac;
class TablaJoc;
class Piesa;
class Dictionar;


struct move {
	std::string ataseazaLitere;
	std::vector<std::string> words;
	std::vector<int> scores;
};

enum direction { HORIZONTAL, VERTICAL, NODIR };

class Player {
private:
	int scor;
	std::string name;

	Tablita*  tablita;
	TablaJoc& tablaJoc;
	Sac&   sac;

	std::vector<struct move> moves;

	struct move ataseazaLitere(std::string litere, int index, direction dir);
	bool poatePuneLitera(std::string litere, int index, direction dir);
	void getInput(std::string& litere, int& locationNdx, char& direction);
	void fillTablita();
	void stergeTabla(std::string& cuvant, int & indexLocatie, char& directie);

	static direction charToDirection(char c);
public:
	Player(std::string s, TablaJoc& tablaJoc, Sac& sac);
	~Player();
	std::string getName();
	void Joaca();
};


#endif