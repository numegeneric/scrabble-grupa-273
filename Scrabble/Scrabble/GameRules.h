#pragma once
#include <iostream>
#include <string>
#include<fstream>
#include"Zona.h"
#include"Generate_random_string.h"
#include"Piesa.h"
#include"TablaJoc.h"


class GameRules
{
public:
	GameRules();
	~GameRules();

	bool wordValidation(const std::string&);//checks if the input word is in the database
	void wordPlacement();
	void letterPlacement(int coordX, int coordY, std::string letter, int score);
};

