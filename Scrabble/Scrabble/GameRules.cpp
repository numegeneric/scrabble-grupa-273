#include "GameRules.h"



GameRules::GameRules()
{
}


GameRules::~GameRules()
{
}
/*
	Returns true if the word is valid
	Returns false if the word is not valid

	Requires further optimisation and way to deal with UPPERCASE based inputs(It dosnt recognize words with uppercase letters in them.)
*/
bool GameRules::wordValidation(const std::string &wordToValidate)
{
	if (wordToValidate.size() == 1)
	{
		std::cout << "Nu exista cuvinte de o litera.";//Eliminates one letter entries.
		return false;
	}
	std::ifstream validWords("cuvinte_scrabble.txt");//Initializes the file for reading opperations
	if (validWords.is_open())
	{
		std::string testWord;
		std::getline(validWords, testWord);
		while (wordToValidate[0] != testWord[0])//First letter validation
		{
			if (validWords.eof())
			{
				return false;
			}
			std::getline(validWords, testWord);
		}
		while ((wordToValidate[0] != testWord[0]) && (wordToValidate[1] != testWord[1]))//First two letter validation
		{
			if (validWords.eof())
			{
				return false;
			}
			std::getline(validWords, testWord);
		}
		while (wordToValidate != testWord)//Complet word validation
		{
			if (validWords.eof())
			{
				return false;
			}
			std::getline(validWords, testWord);
		}
		if (wordToValidate == testWord)
			return true;
	}
	else
	{
		std::cout << "Eroare la deschiderea fisierului.";
	}
	return false;

}

void GameRules::wordPlacement()
{
	//Further discusion required to assure implementation parity
}

void GameRules::letterPlacement(int coordX, int coordY, std::string letter, int score)
{
	Piesa tile(letter, score);
	Zona square(coordX, coordY);
	tile.setZona(&square);
	square.setPiesa(&tile);
}
