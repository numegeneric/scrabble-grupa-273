#pragma once

#include <iostream>
#include<string>
#include<iomanip>
#include"Zona.h"
#include"Sac.h"
#include"EnumMultiplicatori.h"

class Zona;
class Sac;
class TablaJoc
{
public:
	TablaJoc();
	~TablaJoc();
	void arataTabla();
	Piesa* getPiesa(int index) const;
	Zona* getZona(int index) const;
	bool adaugalaZona(int, int, Piesa*);
	bool stergelaZona(int, int);
	bool adaugalaZona(int, Piesa*);
	bool stergelaZona(int);
	bool poatePuneLitera(std::string cuvant, int index, int dir)const;
	void asazaTabla(Sac* sac, std::vector<std::string>);
	Multiplicatori getMultiplicator(int index);
	bool primulCuvant();
	void setPrimulCuvant(bool value);

private:
	static const int NrZone = 225;
	static const int NrRanduri = 15;
	std::vector<Zona *>zone;
	static int getX(int);
	static int getY(int);
	static int getI(int, int);
	mutable bool m_primulCuvant;
	
};

