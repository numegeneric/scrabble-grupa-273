#include <iostream>
#include <cctype>
#include <string>
#include <map>

int letterValue(char ch)
{
	// Map for storing all the valid letters, and their points.
	static const std::map<char, int> charValues = {
		{'a',1}, {'e',1}, {'i',1}, {'l',1}, {'o',1},
		{'r',1}, {'s',1}, {'t',1}, {'u',1}, {'n',1},
		{'d',2}, {'g',2}, {'b',3}, {'c',3}, {'m',3},
		{'p',3}, {'f',4}, {'h',4}, {'v',4}, {'w',4},
		{'y',4}, {'k',5}, {'j',8}, {'x',8}, {'q',10},
		{'z',10}
	};

	//Test in case the letter does not appear in the map.
	auto iter = charValues.find(std::tolower(ch));
	return (iter != std::end(charValues)) ? iter->second : 0;
}

int gameScore(const std::string & word) 
		 
{
	
	const std::size_t length = word.length();

	int total = 0;
	for (std::size_t i = 0; i < length; i++)
	{
		total += letterValue(word[i]);
	}

	return total;
}
#pragma once
