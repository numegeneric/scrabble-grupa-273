#include "Zona.h"


Zona::Zona(int coordX, int coordY) :
	m_coordX(coordX),
	m_coordY(coordY),
	m_multiplicator(Multiplicatori::NONE),
	m_piesa(nullptr)
{
}

Zona::~Zona()
{
}

void Zona::afiseazaZona() const
{
	if (m_piesa)
		std::cout << CYAN << std::setw(4) << m_piesa->getLitera() << RESET << std::setw(4);
	else
	{
		switch (m_multiplicator)
		{
		case DOUBLE_LETTER:
		{
			std::cout << BLUE << std::setw(4) << m_coordY * nrRows + m_coordX << RESET << std::setw(4);
			break;
		}
		case TRIPLE_LETTER:
		{
			std::cout << GREEN << std::setw(4) << m_coordY * nrRows + m_coordX << RESET << std::setw(4);
			break;
		}
		case DOUBLE_WORD:
		{
			std::cout << RED << std::setw(4) << m_coordY * nrRows + m_coordX << RESET << std::setw(4);
			break;
		}
		case TRIPLE_WORD:
		{
			std::cout << YELLOW << std::setw(4) << m_coordY * nrRows + m_coordX << RESET << std::setw(4);
			break;
		}
		case NONE:
		{
			std::cout << std::setw(4) << m_coordY * nrRows + m_coordX << std::setw(4);
			break;
		}
		default:
			break;
		}
	}
}

void Zona::setPiesa(Piesa * piesa)
{
	m_piesa = piesa;
}

Piesa * Zona::getPiesa()
{
	return m_piesa;
}

void Zona::setMultiplicator(Multiplicatori multiplicator)
{
	m_multiplicator = multiplicator;
}

Multiplicatori Zona::getMultiplicator() const
{
	return m_multiplicator;
}
