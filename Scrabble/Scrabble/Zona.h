#pragma once
#include<iostream>
#include<iomanip>
#include"EnumMultiplicatori.h"
#include"Piesa.h"
#include"Culori.h"

class Piesa;
const int nrRows = 15;
class Zona
{
public:
	Zona(int, int);
	~Zona();
	void afiseazaZona() const;
	void setPiesa(Piesa* piesa);
	Piesa * getPiesa();
	void setMultiplicator(Multiplicatori);
	Multiplicatori getMultiplicator()const;

private:
	int m_coordX;
	int m_coordY;
	Piesa * m_piesa;
	Multiplicatori m_multiplicator;
};

