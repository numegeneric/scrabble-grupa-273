#ifndef JOC_H
#define JOC_H

#include<iostream>
#include<vector>
#include"Dictionar.h"

class Player;
class TablaJoc;
class Tablita;
class Sac;

class Joc {
	TablaJoc* tablaJoc;
	Sac* sac;
	std::vector<Player*> players;
public:
	Joc();
	~Joc();
	void begin();
};

#endif
#pragma once
