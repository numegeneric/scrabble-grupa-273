#include "Tablita.h"
#include "Sac.h"
#include "Culori.h"


Tablita::Tablita()
{
	//saculet gol
	for (int i = 0; i < kTablitaSize; i++)
	{
		piese[i] = nullptr;
	}
}

Tablita::Tablita(Sac& sac)
{

	for (int i = 0; i < kTablitaSize; i++)
	{
		piese.push_back(nullptr);
	}

	for (int i = 0; i < kTablitaSize; i++)
	{
		addPiesa(sac.getPiesa());
	}

}


Tablita::~Tablita()
{
	for (int i = 0; i < kTablitaSize; i++)
	{
		piese[i] = nullptr;
	}
}

void Tablita::show() const
{
	for (int i = 0; i < kTablitaSize; i++)
		std::cout << "---";
	std::cout << std::endl;

	for (int i = 0; i < kTablitaSize; i++)
	{
		if (piese[i] != nullptr)
		{
			std::cout << " " << piese[i]->getLitera() << " ";
		}
		else
		{
			std::cout << "   ";
		}
	}
	std::cout << std::endl;

	for (int i = 0; i < kTablitaSize; i++)
	{
		if (piese[i] != nullptr)
		{
			std::cout << BOLDRED << std::setw(2) << piese[i]->getScor() << " " << RESET;
		}
		else
		{
			std::cout << "   ";
		}
	}
	std::cout << std::endl;

	for (int i = 0; i < kTablitaSize; i++)
		std::cout << "---";
	std::cout << std::endl;
}

bool Tablita::addPiesa(Piesa* t)
{
	for (int i = 0; i < kTablitaSize; i++)
	{
		if (piese[i] == nullptr)
		{
			piese[i] = t;

			return true;
		}
	}

	return false;
}

Piesa* Tablita::getPiesa(const int& index)
{
	if (index < 0 || index > kTablitaSize - 1)
	{
		std::cout << "Invalid" << std::endl;
		return nullptr;
	}
	else
	{
		Piesa* temp = piese[index];
		piese[index] = nullptr;

		return temp;
	}
}

Piesa* Tablita::showPiesa(const std::string& c) const
{
	for (int i = 0; i < kTablitaSize; i++)
		if (piese[i]->getLitera() == c)
		{
			return piese[i];
		}
	return nullptr;
}

Piesa* Tablita::showPiesa(const int& index) const
{
	return piese[index];
}

Piesa* Tablita::getPiesa(const std::string& c)
{
	for (int i = 0; i < kTablitaSize; i++)
		if (piese[i] != nullptr && piese[i]->getLitera() == c)
		{
			Piesa* temp = piese[i];
			piese[i] = nullptr;

			return temp;
		}

	return nullptr;
}

// void Tablita::selfTest() const
// {
	// //cout << "Testam tablita  ... ";

	// int n = 0;
	// for (int i = 0; i < kTablitaSize; i++)
	// {
		// if (piese[i] != nullptr)
		// {
			// n++;
		// }
	// }
	// //cout << "test reusit" << endl;
// }

bool Tablita::hasPiesa(const std::string& litera)
{
	for (int i = 0; i < kTablitaSize; i++)
	{
		if (piese[i]->getLitera() == litera)
			return true;
	}
	return false;

}

std::string Tablita::getCaractere() const
{
	std::string s = "";
	for (int i = 0; i < kTablitaSize; i++)
		if (piese[i])
			s += piese[i]->getLitera();
		else
			s += '0';

	return s;
}

bool Tablita::hasCaractere(const std::string& litere) const
{
	std::string tablitaCaractere = getCaractere();
	size_t i;

	for (auto c : litere)
	{
		i = tablitaCaractere.find(c);
		if (i == std::string::npos) //nu e in sir
			return false;
		else
			tablitaCaractere[i] = '0'; //sterge din sir
	}

	return true;
}

int Tablita::getNumPiese() const
{
	int numPiese = 0;
	for (int i = 0; i < kTablitaSize; i++)
		if (piese[i] != nullptr)
			numPiese++;
	return numPiese;
}