#include "Sac.h"



Sac::Sac() :
	m_nrPiese(nrLetters)
{
	srand(time(nullptr));
	std::vector<std::tuple<std::string, int, int>> temp{
		/*std::tuple<std::string,int,int>(" ",2,0),*/std::tuple<std::string,int,int>("a",9,1),std::tuple<std::string,int,int>("b",2,3),
		std::tuple<std::string,int,int>("c",2,3),std::tuple<std::string,int,int>("d",4,2),std::tuple<std::string,int,int>("e",12,1),
		std::tuple<std::string,int,int>("f",2,4),std::tuple<std::string,int,int>("g",3,2),std::tuple<std::string,int,int>("h",2,4),
		std::tuple<std::string,int,int>("i",9,1),std::tuple<std::string,int,int>("j",1,8),std::tuple<std::string,int,int>("k",1,5),
		std::tuple<std::string,int,int>("l",4,1),std::tuple<std::string,int,int>("m",2,3),std::tuple<std::string,int,int>("n",6,1),
		std::tuple<std::string,int,int>("o",8,1),std::tuple<std::string,int,int>("p",2,3),std::tuple<std::string,int,int>("q",1,10),
		std::tuple<std::string,int,int>("r",6,1),std::tuple<std::string,int,int>("s",4,1),std::tuple<std::string,int,int>("t",6,1),
		std::tuple<std::string,int,int>("u",4,1),std::tuple<std::string,int,int>("v",2,4),std::tuple<std::string,int,int>("w",2,4),
		std::tuple<std::string,int,int>("x",1,8),std::tuple<std::string,int,int>("y",2,4),std::tuple<std::string,int,int>("z",1,10)
	};
	int nrPiese;
	for (int index1 = 0; index1 < nrLetters; index1++)
	{
		Piesa tempo(std::get<0>(temp[index1]), std::get<2>(temp[index1]));
		int nrTip = std::get<1>(temp[index1]);
		std::pair <Piesa, int> auxiliar(tempo, nrTip);
		m_piese.push_back(auxiliar);
	}
}


Sac::~Sac()
{
}

void Sac::afisarePiese()
{
	for (int index = 0; index < m_nrPiese; index++)
	{
		std::cout << " " << m_piese[index].first.getLitera() << " " << std::endl;
	}
}

Piesa * Sac::getPiesa()
{
	if (m_nrPiese)
	{
		int index;
		do
		{
			index = rand() % m_nrPiese;
		} while (m_piese[index].second == 0);
		//Piesa piesa = m_piese[index].first;
		m_piese[index].second--;
		/*if (m_piese[index].second == 0)
		{
			int nrTip = m_piese[index].second;
			m_piese[index] = m_piese[m_nrPiese - 1];
			m_piese[m_nrPiese - 1].first = piesa;
			m_piese[m_nrPiese - 1].second = nrTip;
			m_nrPiese--;
		}*/
		return &m_piese[index].first;
	}
	else
		return nullptr;
}

int Sac::getnrPiese() const
{
	return m_nrPiese;
}

void Sac::addPiesa(Piesa *piesa)
{
	if (m_nrPiese != nrLetters)
	{
		for (int index = 0; index < m_piese.size(); index++)
		{
			Piesa ver = *piesa;
			if (m_piese[index].first == ver)
			{
				if (m_piese[index].second == 0)
				{
					m_piese[index].second++;
					m_nrPiese++;
				}
				else
				{
					m_piese[index].second++;
				}
			}
		}
	}
}

void Sac::reset()
{
	m_nrPiese = 100;
}

Piesa * Sac::getPiesa(std::string litera)
{
	int locatie = 0;
	bool gasit = false;
	Piesa *piesa = nullptr;
	for (int index = 0; index < m_nrPiese; index++)
	{
		piesa = &m_piese[index].first;
		if (piesa->getLitera() == litera)
		{
			gasit = true;
			locatie = index;
			m_piese[index].second--;
			break;
		}
	}
	if (gasit)
	{
		if (m_piese[locatie].second == 0)
		{
			m_piese[locatie] = m_piese[m_nrPiese - 1];
			m_piese[m_nrPiese - 1].first = *piesa;
			m_nrPiese--;
		}
	}
	return piesa;
}
