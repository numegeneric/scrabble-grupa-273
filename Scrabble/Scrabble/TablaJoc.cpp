#include "TablaJoc.h"



TablaJoc::TablaJoc():
	m_primulCuvant(false)
{
	for (int i = 0; i < NrZone; i++)
	{
		zone.push_back(new Zona(i%NrRanduri,i/NrRanduri));
	}
	std::vector<int> dLitera{ 3,11,45,165,59,179,52,36,38,108,92,122,172,186,188,116,102,132,96,98,126,128 };
	for (int index : dLitera)
		zone[index]->setMultiplicator(DOUBLE_LETTER);
	std::vector<int> tLitera{ 20,24,76,136,200,204,88,148,80,84,140,144 };
	for (int index : tLitera)
		zone[index]->setMultiplicator(TRIPLE_LETTER);
	std::vector<int> dCuvant{ 16,32,48,64,28,42,56,70,112,196,182,168,154,208,192,176,160 };
	for (int index : dCuvant)
		zone[index]->setMultiplicator(DOUBLE_WORD);
	std::vector<int> tCuvant{ 0,7,14,105,119,210,217,224 };
	for (int index : tCuvant)
		zone[index]->setMultiplicator(TRIPLE_WORD);
}


TablaJoc::~TablaJoc()
{
}

void TablaJoc::arataTabla()
{
	std::cout << "\t";
	for (int index = 0; index < NrRanduri; index++)
		std::cout << std::setw(5) << index <<"\t";
	std::cout << std::endl;
	std::cout << "\t";
	for (int index = 0; index < NrRanduri; index++)
		std::cout << "+-------";
	std::cout <<"+"<< std::endl;

	for (int index = 0; index < NrZone; index++)
	{
		if (index % NrRanduri == 0)
			std::cout << (index / NrRanduri) << "\t";

		std::cout << "|";

		zone[index]->afiseazaZona();

		if ((index + 1) % NrRanduri == 0)
		{
			std::cout << "|" << std::endl;
			std::cout << " \t";
			for (int j = 0; j < NrRanduri; j++)
				std::cout << "+-------";
			std::cout << "+" << std::endl;
		}

	}
}

Zona * TablaJoc::getZona(int index) const
{
	return zone[index];
}

bool TablaJoc::adaugalaZona(int coordX, int coordY, Piesa * piesa)
{
	if (coordX < 0 || coordX>NrRanduri-1 || coordY < 0 || coordY>NrRanduri-1)
	{
		std::cout << "Index invalid.";
		return false;
	}
	if (zone[coordY * NrRanduri + coordX]->getPiesa() == nullptr)
	{
		zone[coordY * NrRanduri + coordX]->setPiesa(piesa);
		return true;
	}
	return false;
}

bool TablaJoc::stergelaZona(int coordX, int coordY)
{
	zone[coordY * NrRanduri + coordX]->setPiesa(nullptr);
	return true;
}

bool TablaJoc::adaugalaZona(int index , Piesa * piesa)
{
	return adaugalaZona(index % NrRanduri, index / NrRanduri, piesa);
}

bool TablaJoc::stergelaZona(int index)
{
	return stergelaZona(index % NrRanduri, index / NrRanduri);
}

bool TablaJoc::poatePuneLitera(std::string cuvant, int index, int dir) const
{
	bool intersect = false;      
	bool through_center = false; 
	bool sizeok = false;
	int orgindex = index;

	unsigned int placed = 0;     

	if ((dir == 0 && index % NrRanduri != 0 && zone[index - 1]->getPiesa()) || (index / NrRanduri != 0 && zone[index-1]->getPiesa()))
		intersect = true;

	while (1)
	{
		if ((dir == 0 && index / NrRanduri != orgindex / NrRanduri) || index > 224 || placed == cuvant.size())
			break;
		if (index == 112)
			through_center = true;
		if (zone[index]->getPiesa())
			intersect = true;
		else
		{
			if (dir == 0 && ((index / NrRanduri != 0 && zone[index - NrRanduri]->getPiesa()) || (index / NrRanduri != NrRanduri-1 && zone[index + NrRanduri]->getPiesa())))
				intersect = true;
			if ((index % NrRanduri != 0 && zone[index - 1]->getPiesa()) || (index % NrRanduri != NrRanduri-1 && zone[index + 1]->getPiesa()))
				intersect = true;
			placed++;
		}
		if (dir == 0) 
			index++;
		else        
			index += NrRanduri;
	}

	if (index >= 0 && index <= 224 && zone[index]->getPiesa())
		intersect = true;
	sizeok = (placed == cuvant.size());

	if (!m_primulCuvant && through_center && sizeok)
	{
		m_primulCuvant = true;
		return true;
	}

	return sizeok && intersect;
}

void TablaJoc::asazaTabla(Sac * sac, std::vector<std::string> randuri)
{
	Piesa* piesa;
	for(int index=0;index<NrRanduri;index++)
		for(int index1=0;index1<NrRanduri;index1++)
			if (randuri[index][index1] != '0')
			{
				piesa = sac->getPiesa(randuri[index]);
				zone[getI(index1, index)]->setPiesa(piesa);
				piesa->setZona(zone[getI(index1, index)]);
			}
}

int TablaJoc::getX(int index)
{
	return index%NrRanduri;
}

int TablaJoc::getY(int index)
{
	return index/NrRanduri;
}

int TablaJoc::getI(int x, int y)
{
	return y*NrRanduri+x;
}

Multiplicatori TablaJoc::getMultiplicator(int index)
{
	return zone[index]->getMultiplicator();
 }

bool TablaJoc::primulCuvant()
{
	return m_primulCuvant;
}
Piesa* TablaJoc::getPiesa(int index) const
{
	return zone[index]->getPiesa();
}

void TablaJoc::setPrimulCuvant(bool value) 
{
	m_primulCuvant = value;
}
