#include "Dictionar.h"



Dictionar::Dictionar()
{
}

Dictionar::Dictionar(const int& nrLang)
{
	std::ifstream inWords;
	if (nrLang == 1)
		inWords = std::ifstream("EnDictionary.txt");
	else
		if (nrLang == 2)
			inWords = std::ifstream("RoDictionary.txt");

	std::string readS = " ";
	while (!inWords.eof())
	{
		inWords >> readS;
		m_Words.insert(readS);

	}
	inWords.close();
}


Dictionar::~Dictionar()
{
}

bool Dictionar::checkWord(const std::string & word)
{
		 
	if (m_Words.find(word)!=m_Words.end())
		return true;

	return false;
}
