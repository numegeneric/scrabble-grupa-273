#include "Piesa.h"






Piesa::Piesa(std::string litera, int scor) :
	m_litera(litera),
	m_scor(scor),
	m_zona(nullptr)
{
}

Piesa::~Piesa()
{
}

Zona* Piesa::arataZona() const
{
	return m_zona;
}

std::string Piesa::getLitera()
{
	return m_litera;
}

int Piesa::getScor()
{
	return m_scor;
}

void Piesa::setZona(Zona * zona)
{
	m_zona = zona;
}

bool Piesa::operator ==(Piesa other)
{
	if ((this->m_litera == other.m_litera) && (this->m_scor == other.m_scor) && (this->m_zona == other.m_zona))
		return true;
	return false;
}
